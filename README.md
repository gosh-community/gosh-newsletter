# GOSH Newsletter Archive

## 2024
* [August 2024](https://gosh-community.gitlab.io/gosh-newsletter/2024-08-August-Newsletter.html)
* [June 2024](https://gosh-community.gitlab.io/gosh-newsletter/2024-06-June-Newsletter.html)
* [May 2024](https://gosh-community.gitlab.io/gosh-newsletter/2024-05-May-Newsletter.html)
* [April 2024](https://gosh-community.gitlab.io/gosh-newsletter/2024-04-April-Newsletter.html)
* [March 2024](https://gosh-community.gitlab.io/gosh-newsletter/2024-03-March-Newsletter.html)
* [February 2024](https://gosh-community.gitlab.io/gosh-newsletter/2024-02-February-Newsletter.html)
* [January 2024](https://gosh-community.gitlab.io/gosh-newsletter/2024-01-January-Newsletter.html)

## 2023

* [January 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-01-January-Newsletter.html)
* [February 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-02-February-Newsletter.html)
* [March 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-03-March-Newsletter.html)
* [April 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-04-April-Newsletter.html)
* [May 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-05-May-Newsletter.html)
* [June 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-06-June-Newsletter.html)
* [July 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-07-July-Newsletter.html)
* [August 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-08-August-Newsletter.html)
* [September 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-09-September-Newsletter.html)
* [October 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-10-October-Newsletter.html)
* [December 2023](https://gosh-community.gitlab.io/gosh-newsletter/2023-12-December-Newsletter.html)


## 2022

* [January 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-01-January-Newsletter.html)
* [February 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-02-February-Newsletter.html)
* [March 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-03-March-Newsletter.html)
* [April 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-04-April-Newsletter.html)
* [May 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-05-May-Newsletter.html)
* [June 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-06-June-Newsletter.html)
* [July 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-07-July-Newsletter.html)
* [August 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-08-August-Newsletter.html)
* [September 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-09-September-Newsletter.html)
* [October 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-10-October-Newsletter.html)
* [November 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-11-November-Newsletter.html)
* [December 2022](https://gosh-community.gitlab.io/gosh-newsletter/2022-12-December-Newsletter.html)

## 2021
* [November 2021](https://gosh-community.gitlab.io/gosh-newsletter/2021-11-November-Newsletter.html)
* [December 2021](https://gosh-community.gitlab.io/gosh-newsletter/2021-12-December-Newsletter.html)


## About the archive

This is an archive of the monthly GOSH Community Newsletters, along with the HTML code used to create each one.

You can sign up to the newsletter here: https://openhardware.science/gosh-monthly-newsletter/

All photos used in the newsletter are under public domain and can be accesed via [this repository](https://gitlab.com/gosh-community/gosh-newsletter/-/tree/main/newsletter%20images). The photos original source is the [GOSH Community Flickr](https://www.flickr.com/photos/goshcommunity/albums/with/72177720303226220)

## GOSH Newsletter Cover Photo Template

You can access the svg file and images used to create the GOSH newsletter cover photos in [this directory](https://gitlab.com/gosh-community/gosh-newsletter/-/tree/main/newsletter%20cover%20photos).
